/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the <fitsio.h> header file. */
/* #undef HAVE_FITSIO_H */

/* Define to 1 if you have the <inttypes.h> header file. */
/* #undef HAVE_INTTYPES_H */

/* Define to 1 if you have the `cfitsio' library (-lcfitsio). */
/* #undef HAVE_LIBCFITSIO */

/* Define to 1 if you have the `tiff' library (-ltiff). */
/* #undef HAVE_LIBTIFF */

/* Define to 1 if you have the <memory.h> header file. */
/* #undef HAVE_MEMORY_H */

/* Define to 1 if you have the <stdint.h> header file. */
/* #undef HAVE_STDINT_H */

/* Define to 1 if you have the <stdlib.h> header file. */
/* #undef HAVE_STDLIB_H */

/* Define to 1 if you have the <strings.h> header file. */
/* #undef HAVE_STRINGS_H */

/* Define to 1 if you have the <string.h> header file. */
/* #undef HAVE_STRING_H */

/* Define to 1 if you have the <sys/stat.h> header file. */
/* #undef HAVE_SYS_STAT_H */

/* Define to 1 if you have the <sys/types.h> header file. */
/* #undef HAVE_SYS_TYPES_H */

/* Define to 1 if you have the <tiffio.h> header file. */
/* #undef HAVE_TIFFIO_H */

/* Define to 1 if you have the <tiff.h> header file. */
/* #undef HAVE_TIFF_H */

/* Define to 1 if you have the <unistd.h> header file. */
/* #undef HAVE_UNISTD_H */

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Name of package */
#define PACKAGE "qsiapi"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "support@qsimaging.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "qsiapi"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "qsiapi 7.6.0"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "qsiapi"

/* Define to the home page for this package. */
#define PACKAGE_URL "www.qsimaging.com"

/* Define to the version of this package. */
#define PACKAGE_VERSION "7.6.0"

/* Define to 1 if you have the ANSI C header files. */
/* #undef STDC_HEADERS */

/* Version number of package */
#define VERSION "7.6.0"
