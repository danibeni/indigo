# Calar Alto Weather Station driver

## Supported devices

Calar Alto weather station device.

Single device is present on startup (no hot-plug support).

## Supported platforms

This driver is platform independent.

## License

INDIGO Astronomy open-source license.

## Use

indigo_server indigo_weather_ca

## Status: Stable

## Comments
