// Copyright (c) 2017 Rumen G. Bogdanovski
// All rights reserved.
//
// You can use this software under the terms of 'INDIGO Astronomy
// open-source license' (see LICENSE.md).
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHORS 'AS IS' AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// version history
// 2.0 by Rumen G. Bogdanovski

/** INDIGO GPS Simulator driver
 \file indigo_gps_simulator.h
 */

#ifndef aux_weather_ca_h
#define aux_weather_ca_h

#include <indigo/indigo_driver.h>
#include <indigo/indigo_aux_driver.h>

#ifdef __cplusplus
extern "C" {
#endif


#define WEATHER_CA_NAME      "Calar Alto Weather Station"

/** Main site group name string.
 */
#define WEATHER_SITE_GROUP															"Weather Site"

#define AUX_WEATHER_SITE_STATUS_WARN_ITEM_NAME					"BAD_WEATHER_WARNING"

#define AUX_WEATHER_SITE_STATUS_ALERT_ITEM_NAME					"BAD_WEATHER_ALERT"

#define AUX_WEATHER_SITE_STATUS_PROPERTY_NAME           "WEATHER_STATUS"


/** Device context pointer.
 */
#define WEATHER_CONTEXT																((indigo_weather_context *)device->device_context)

//-------------------------------------------
/** WEATHER_SITE_DATA_PROPERTY property pointer, property is mandatory, property change request should be fully handled by device driver.
 */
#define WEATHER_SITE_DATA_PROPERTY					(WEATHER_CONTEXT->weather_site_data_property)

/** WEATHER_SITE_DATA_PROPERTY.TEMPERATURE property item pointer.
 */
#define WEATHER_SITE_TEMPERATURE_ITEM		(WEATHER_SITE_DATA_PROPERTY->items+0)

/** WEATHER_SITE_DATA_PROPERTY.HUMIDITY property item pointer.
 */
#define WEATHER_SITE_HUMIDITY_ITEM		(WEATHER_SITE_DATA_PROPERTY->items+1)


//-------------------------------------------
/** WEATHER_SITE_DATA_PROPERTY property pointer, property is mandatory, property change request should be fully handled by device driver.
 */
#define WEATHER_SITE_STATUS_PROPERTY					(WEATHER_CONTEXT->weather_site_alert_property)

#define WEATHER_SITE_STATUS_WARN_ITEM  (WEATHER_SITE_STATUS_PROPERTY->items+0)

#define WEATHER_SITE_STATUS_ALERT_ITEM  (WEATHER_SITE_STATUS_PROPERTY->items+1)

//------------------------------------------------
/** Weather device context structure.
 */
typedef struct {
	indigo_device_context device_context;                   ///< device context base
	indigo_property *weather_site_data_property;						///< Weather site data pointer
	indigo_property *weather_site_alert_property;						///< Weather site alert pointer
} indigo_weather_context;


/** Create Calar Alto weather station device instance
 */

extern indigo_result indigo_aux_weather_ca(indigo_driver_action action, indigo_driver_info *info);

#ifdef __cplusplus
}
#endif

#endif /* weather_ca_h */
