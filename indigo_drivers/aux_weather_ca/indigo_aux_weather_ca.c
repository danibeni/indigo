// Copyright (c) 2017 Rumen G. Bogdanovski
// All rights reserved.
//
// You can use this software under the terms of 'INDIGO Astronomy
// open-source license' (see LICENSE.md).
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHORS 'AS IS' AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


// version history
// 2.0 by Rumen G. Bogdanovski

/** INDIGO Calar Alto Weather Station driver
 \file indigo_aux_weather_simulator_ca.c
 */

#define DRIVER_VERSION 0x0001
#define DRIVER_NAME	"indigo_aux_weather_ca"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>
#include <assert.h>

#include <indigo/indigo_driver_json.h>
#include "indigo_aux_weather_ca.h"


/* Driver constants */
#define REFRESH_SECONDS    (1.0)
#define WEATHER_LAST_FILE_PATH	 "/meteo/WDX/last.wdx"
#define WEATHER_TELWARN_FILE_PATH	"/meteo/WDX/CLOSE/.tel12Warn"
#define WEATHER_TELCLOSE_FILE_PATH	"/meteo/WDX/CLOSE/.tel12Close"

#define PRIVATE_DATA        ((simulator_private_data *)device->private_data)

// gp_bits is used as boolean
#define is_connected                   gp_bits

typedef struct {
	int timer_ticks;
	indigo_timer *weather_timer;
} simulator_private_data;

// -------------------------------------------------------------------------------- INDIGO Calar Alto Weather Station device implementation
static bool weather_sta_open(indigo_device *device) {
	if (device->is_connected) return false;
	if (indigo_try_global_lock(device) != INDIGO_OK) {
		CONNECTION_PROPERTY->state = INDIGO_ALERT_STATE;
		indigo_update_property(device, CONNECTION_PROPERTY, "Device is locked");
		return INDIGO_OK;
	}
	srand((unsigned)time(NULL));
	PRIVATE_DATA->timer_ticks = 0;
	return true;
}


static void weather_sta_close(indigo_device *device) {
	if (!device->is_connected) return;
	indigo_global_unlock(device);
	PRIVATE_DATA->timer_ticks = 0;
}

static void weather_callback(indigo_device *device) {
	char weather_file_line[100];
	char delim[] = " ";
	char *weather_data_ptr[16];
	int i = 0;
	FILE *weather_file = fopen(WEATHER_LAST_FILE_PATH, "r");
	if (weather_file != NULL) {
		fscanf(weather_file, "%[^\n]", weather_file_line);
		weather_data_ptr[i] = strtok(weather_file_line, delim);
		while (weather_data_ptr[i] != NULL) {
			weather_data_ptr[++i] = strtok(NULL, delim);
			switch(i) {
				case 7:
					WEATHER_SITE_TEMPERATURE_ITEM->number.value = atof(weather_data_ptr[i]);
					break;
				case 8:
					WEATHER_SITE_HUMIDITY_ITEM->number.value = atof(weather_data_ptr[i]);
					break;
				default:
					break;
			}

		}
		fclose(weather_file);
		FILE *telwarn_file = fopen(WEATHER_TELWARN_FILE_PATH, "r");
		if (telwarn_file != NULL) {
			WEATHER_SITE_STATUS_WARN_ITEM->light.value = INDIGO_ALERT_STATE;
			fclose(telwarn_file);
		} else {
			WEATHER_SITE_STATUS_WARN_ITEM->light.value = INDIGO_OK_STATE;
		}
		FILE *telclose_file = fopen(WEATHER_TELCLOSE_FILE_PATH, "r");
		if (telclose_file != NULL) {
			WEATHER_SITE_STATUS_ALERT_ITEM->light.value = INDIGO_ALERT_STATE;
			fclose(telclose_file);
		} else {
			WEATHER_SITE_STATUS_ALERT_ITEM->light.value = INDIGO_OK_STATE;
		}

		indigo_update_property(device, WEATHER_SITE_DATA_PROPERTY, NULL);
		indigo_update_property(device, WEATHER_SITE_STATUS_PROPERTY, NULL);
	}
		indigo_reschedule_timer(device, REFRESH_SECONDS, &PRIVATE_DATA->weather_timer);
}

static indigo_result aux_weather_enumerate_properties(indigo_device *device, indigo_client *client, indigo_property *property);

static indigo_result aux_weather_attach(indigo_device *device) {
	assert(device != NULL);
	assert(PRIVATE_DATA != NULL);
	if (WEATHER_CONTEXT == NULL) {
		device->device_context = malloc(sizeof(indigo_weather_context));
		assert(device->device_context);
		memset(device->device_context, 0, sizeof(indigo_weather_context));
	}
	if (WEATHER_CONTEXT != NULL) {
		if (indigo_aux_attach(device, DRIVER_VERSION, INDIGO_INTERFACE_WEATHER) == INDIGO_OK) {

			SIMULATION_PROPERTY->hidden = true;
			DEVICE_PORT_PROPERTY->hidden = true;
			DEVICE_PORTS_PROPERTY->hidden = true;

			// -------------------------------------------------------------------------------- WEATHER_SITE_DATA
			WEATHER_SITE_DATA_PROPERTY = indigo_init_number_property(NULL, device->name, AUX_WEATHER_PROPERTY_NAME, WEATHER_SITE_GROUP, "Weather", INDIGO_OK_STATE, INDIGO_RO_PERM, 2);
			if (WEATHER_SITE_DATA_PROPERTY == NULL)
				return INDIGO_FAILED;
			WEATHER_SITE_DATA_PROPERTY->hidden = false;
			WEATHER_SITE_DATA_PROPERTY->count = 2;
			indigo_init_number_item(WEATHER_SITE_TEMPERATURE_ITEM, AUX_WEATHER_TEMPERATURE_ITEM_NAME, "Temperature (ºC)", -30, 50, 0, 0);
			indigo_init_number_item(WEATHER_SITE_HUMIDITY_ITEM, AUX_WEATHER_HUMIDITY_ITEM_NAME, "Humidity (%RH)", 0, 100, 0, 0);

			WEATHER_SITE_STATUS_PROPERTY = indigo_init_light_property(NULL, device->name, AUX_WEATHER_SITE_STATUS_PROPERTY_NAME, WEATHER_SITE_GROUP, "Weather Status",INDIGO_OK_STATE, 2);
			if (WEATHER_SITE_STATUS_PROPERTY == NULL)
				return INDIGO_FAILED;
			indigo_init_light_item(WEATHER_SITE_STATUS_WARN_ITEM, AUX_WEATHER_SITE_STATUS_WARN_ITEM_NAME, "Bad Weather Warning",INDIGO_IDLE_STATE);
			indigo_init_light_item(WEATHER_SITE_STATUS_ALERT_ITEM, AUX_WEATHER_SITE_STATUS_ALERT_ITEM_NAME, "Bad Weather Alert",INDIGO_IDLE_STATE);

			INDIGO_DEVICE_ATTACH_LOG(DRIVER_NAME, device->name);
			return aux_weather_enumerate_properties(device, NULL, NULL);
		}
	}
	return INDIGO_FAILED;
}

static indigo_result aux_weather_enumerate_properties(indigo_device *device, indigo_client *client, indigo_property *property) {
	assert(device != NULL);
	assert(DEVICE_CONTEXT != NULL);
	indigo_result result = INDIGO_OK;
	if ((result = indigo_aux_enumerate_properties(device, client, property)) == INDIGO_OK) {
		if (indigo_property_match(WEATHER_SITE_DATA_PROPERTY, property))
			indigo_define_property(device, WEATHER_SITE_DATA_PROPERTY, NULL);
		if (indigo_property_match(WEATHER_SITE_STATUS_PROPERTY, property))
			indigo_define_property(device,  WEATHER_SITE_STATUS_PROPERTY, NULL);
	}
	return result;
}


static indigo_result aux_weather_change_property(indigo_device *device, indigo_client *client, indigo_property *property) {
	assert(device != NULL);
	assert(DEVICE_CONTEXT != NULL);
	assert(property != NULL);
	if (indigo_property_match(CONNECTION_PROPERTY, property)) {
			// -------------------------------------------------------------------------------- CONNECTION
			indigo_property_copy_values(CONNECTION_PROPERTY, property, false);
			if (CONNECTION_CONNECTED_ITEM->sw.value) {
				if (!device->is_connected) {
					if (weather_sta_open(device)) {
						CONNECTION_PROPERTY->state = INDIGO_OK_STATE;
						device->is_connected = true;
						indigo_define_property(device, WEATHER_SITE_DATA_PROPERTY, NULL);
						indigo_define_property(device, WEATHER_SITE_STATUS_PROPERTY, NULL);
						/* start updates */
						PRIVATE_DATA->weather_timer = indigo_set_timer(device, REFRESH_SECONDS, weather_callback);
					}
				} else {
					CONNECTION_PROPERTY->state = INDIGO_ALERT_STATE;
					indigo_set_switch(CONNECTION_PROPERTY, CONNECTION_DISCONNECTED_ITEM, true);
				}
			} else {
				indigo_cancel_timer(device, &PRIVATE_DATA->weather_timer);
				weather_sta_close(device);
				device->is_connected = false;
				indigo_delete_property(device, WEATHER_SITE_DATA_PROPERTY, NULL);
				indigo_delete_property(device, WEATHER_SITE_STATUS_PROPERTY, NULL);
				CONNECTION_PROPERTY->state = INDIGO_OK_STATE;
			}
	} else if (indigo_property_match(WEATHER_SITE_DATA_PROPERTY, property)) {
		// -------------------------------------------------------------------------------- GET WEATHER DATA
		indigo_property_copy_values(WEATHER_SITE_DATA_PROPERTY, property, false);
		WEATHER_SITE_DATA_PROPERTY->state = INDIGO_OK_STATE;
		if (IS_CONNECTED) {
			indigo_update_property(device, WEATHER_SITE_DATA_PROPERTY, NULL);
		}
		return INDIGO_OK;
	} else if (indigo_property_match(WEATHER_SITE_STATUS_PROPERTY, property)) {
		// -------------------------------------------------------------------------------- GET WEATHER STATUS
		indigo_property_copy_values(WEATHER_SITE_STATUS_PROPERTY, property, false);
		WEATHER_SITE_STATUS_PROPERTY->state = INDIGO_OK_STATE;
		if (IS_CONNECTED) {
			indigo_update_property(device, WEATHER_SITE_STATUS_PROPERTY, NULL);
		}
		return INDIGO_OK;
	} else if (indigo_property_match(CONFIG_PROPERTY, property)) {
		// -------------------------------------------------------------------------------- CONFIG
		if (indigo_switch_match(CONFIG_SAVE_ITEM, property)) {
			indigo_save_property(device, NULL, WEATHER_SITE_DATA_PROPERTY);
		}
	}
	return indigo_aux_change_property(device, client, property);
}


static indigo_result aux_weather_detach(indigo_device *device) {
	assert(device != NULL);
	if (CONNECTION_CONNECTED_ITEM->sw.value)
		indigo_device_disconnect(NULL, device->name);
	indigo_cancel_timer(device, &PRIVATE_DATA->weather_timer);
	indigo_global_unlock(device);

	indigo_release_property(WEATHER_SITE_DATA_PROPERTY);
	indigo_release_property(WEATHER_SITE_STATUS_PROPERTY);

	INDIGO_DEVICE_DETACH_LOG(DRIVER_NAME, device->name);
	return indigo_aux_detach(device);
}

// --------------------------------------------------------------------------------

static simulator_private_data *private_data = NULL;

static indigo_device *weather = NULL;

indigo_result indigo_aux_weather_ca(indigo_driver_action action, indigo_driver_info *info) {
	static indigo_device weather_template = INDIGO_DEVICE_INITIALIZER(
		WEATHER_CA_NAME,
		aux_weather_attach,
		aux_weather_enumerate_properties,
		aux_weather_change_property,
		NULL,
		aux_weather_detach
	);

	static indigo_driver_action last_action = INDIGO_DRIVER_SHUTDOWN;

	SET_DRIVER_INFO(info, WEATHER_CA_NAME, __FUNCTION__, DRIVER_VERSION, false, last_action);

	if (action == last_action)
		return INDIGO_OK;

	switch (action) {
	case INDIGO_DRIVER_INIT:
		last_action = action;
		private_data = malloc(sizeof(simulator_private_data));
		assert(private_data != NULL);
		memset(private_data, 0, sizeof(simulator_private_data));
		weather = malloc(sizeof(indigo_device));
		assert(weather != NULL);
		memcpy(weather, &weather_template, sizeof(indigo_device));
		weather->private_data = private_data;
		indigo_attach_device(weather);
		break;

	case INDIGO_DRIVER_SHUTDOWN:
		last_action = action;
		if (weather != NULL) {
			indigo_detach_device(weather);
			free(weather);
			weather = NULL;
		}
		if (private_data != NULL) {
			free(private_data);
			private_data = NULL;
		}
		break;

	case INDIGO_DRIVER_INFO:
		break;
	}

	return INDIGO_OK;
}
